<?php

use LaravelBook\Ardent\Ardent;

class Customer extends Ardent{
	protected $table = 'customers';

	public function address(){
		return $this->hasMany('Customeraddr', 'cust_no');
	}

}