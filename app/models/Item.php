<?php

use LaravelBook\Ardent\Ardent;

class Item extends Ardent{
	protected $table = 'items';

	public function category(){
		return $this->belongsTo('Category');
	}

}