<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVendorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vendors', function(Blueprint $table) {
			$table->increments('id')->unique();
			$table->string('vend_no', 10)->unique();
			$table->string('comp_code', 4)->default('01');
			$table->string('vend_grp_code', 20);
			$table->string('vend_name', 200);
			$table->string('curr_code', 10);
			$table->string('tax_type', 4);
			$table->text('addr')->nullable();
			$table->string('zip', 10)->nullable();
			$table->string('ctry_code', 10);
			$table->string('cont_name', 200);
			$table->string('phone_no1', 20);
			$table->string('phone_no2', 20)->nullable();
			$table->string('fax_no', 20)->nullable();
			$table->string('cont_email', 100)->nullable();
			$table->tinyInteger('inactive')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vendors');
	}

}
