<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table) {
			$table->increments('id')->unique();
			$table->string('comp_code', 4)->default('01');
			$table->integer('item_no')->unique();
			$table->text('item_descr')->nullable();
			$table->string('cat_code', 10); //FK for category
			$table->string('uom', 4);
			$table->tinyInteger('is_sell')->default(0);
			$table->tinyInteger('is_buy')->default(0);
			$table->tinyInteger('is_inventory')->default(0);
			$table->text('flang_descr')->nullable();
			$table->tinyInteger('is_new')->default(0);
			$table->tinyInteger('inactive')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
