<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('torders', function(Blueprint $table) {
			$table->increments('id')->unique();
			$table->string('comp_code', 4)->default('01');
			$table->integer('torder_from');
			$table->integer('torder_to');
			$table->text('delivery_addr');
			$table->datetime('delivery_datetime');
			$table->string('delivery_driver');
			$table->string('curr_code')->nulllable();
			$table->decimal('subtotal', 8, 2);
			$table->decimal('gst', 8, 2);
			$table->decimal('total', 8, 2);
			$table->text('remark');
			$table->tinyInteger('inactive')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('torders');
	}

}
