<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomeraddrsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customeraddrs', function(Blueprint $table) {
			$table->increments('id');
			$table->string('comp_code', 4)->default('01');
			$table->string('cust_no', 10); //fk for customer
			$table->integer('addr_ref');
			$table->string('addr', 200);
			$table->string('zip', 20);
			$table->string('ctry_code', 4);
			$table->string('cont_name', 100);
			$table->string('cont_off_tel', 20);
			$table->string('cont_res_tel', 20)->nullable();
			$table->string('cont_hp_no', 20)->nullable();
			$table->string('cont_fax_no', 20)->nullable();
			$table->string('cont_email', 200)->nullable();
			$table->tinyInteger('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customeraddrs');
	}

}
