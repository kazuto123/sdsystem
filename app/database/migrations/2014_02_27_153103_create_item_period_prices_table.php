<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemPeriodPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_period_prices', function(Blueprint $table) {
			$table->increments('id');
			$table->string('comp_code', 4)->default('01');
			$table->integer('item_id');
			$table->string('cust_group_code', 4)->nullable();
			$table->integer('cust_no')->nullable();
			$table->string('uom', 4);
			$table->integer('s_no');
			$table->datetime('from_date');
			$table->datetime('to_date')->nullable();
			$table->decimal('price', 8, 2);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_period_prices');
	}

}
