<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemCustGrpPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_cust_grp_prices', function(Blueprint $table) {
			$table->increments('id');
			$table->string('comp_code', 4)->default('01');
			$table->integer('item_id');
			$table->string('cust_group_code', 4);
			$table->string('uom', 4);
			$table->decimal('price', 8, 2);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_cust_grp_prices');
	}

}
