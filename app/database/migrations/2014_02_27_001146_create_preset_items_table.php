<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePresetItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('preset_items', function(Blueprint $table) {
			$table->increments('id');
			$table->string('preset_by_cust_no');
			$table->string('item_no');  //FK for items
			$table->integer('qty')->unsigned();
			$table->string('uom');
			$table->decimal('unitprice');
			$table->decimal('totalamt');
			$table->tinyInteger('inactive')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('preset_items');
	}

}
