<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemParamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('system_params', function(Blueprint $table) {
			$table->increments('id');
			$table->string('comp_code', 4)->default('01');
			$table->string('param_id', 40);
			$table->string('param_value', 4);
			$table->string('remark')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('system_params');
	}

}
