<?php

//test routing
Route::get('/test/{id}', function($id){
	return "Test #$id success";
})->where('id', '[0-9]+'); //make it onli allow number

//return a view with input from routes
//Route::get('/test/sample1', function(){ return View::make('sample.sample001')->with('num_of_cats', 9000); });

Route::get('/orders/index', array('uses' => 'FakeController@showOrderIndex'));
Route::get('/orders/create', array('uses' => 'FakeController@showCreateOrderForm'));
Route::get('/role/assign/{username}/{rolename}', 'RoleController@assignRole');
Route::get('/role/create/{rolename}', 'RoleController@createRole');
Route::get('/perm/create/{perm_name}/{perm_display_name}', 'RoleController@createPermission');
Route::get('/perm/assign/{rolename}/{perm_name}', 'RoleController@assignPerm2Role');
//Route::get('/sample', function(){ return View::make('sample.sample_bs'); });

//Actual Routes start here --------------------------------------------------------------------

//routes the root to the login page
Route::get('/', function(){ return Redirect::to('/user/login'); });

//---end test routing// Confide routes
Route::get('user/create',                  'UserController@create');
Route::post('user',                        'UserController@store');
Route::get('user/login',                   'UserController@login');
Route::post('user/login',                  'UserController@do_login');
Route::get('user/confirm/{code}',          'UserController@confirm');
Route::get('user/forgot_password',         'UserController@forgot_password');
Route::post('user/forgot_password',        'UserController@do_forgot_password');
Route::get('user/reset_password/{token}',  'UserController@reset_password');
Route::post('user/reset_password',         'UserController@do_reset_password');
Route::get('user/logout',                  'UserController@logout');
Route::get('user/index',      			   'UserController@index');
Route::get('user/auto_insert/{email}/{username}/{password}/{issalesman}/{role}/{id}', 			   
	                                       'UserController@autoInsert');
Route::get('main/dashboard', 			   'HomeController@showDashboard');

//assigned roles
Route::get('role/create', 				   'RoleController@createRoles');

//general routes
Route::get('notlogin', 					   'ErrorController@showNotLogin');
Route::get('notaccessible', 			   'ErrorController@showNotAccessible');
Route::get('pagenotfound', 				   'ErrorController@showPageNotFound');

//SDAPI
Route::get('sdapi/customer', 			   	'SdapiController@API_getCustomer');
Route::get('sdapi/customer_addr', 		   	'SdapiController@API_getCustAddr');
Route::get('sdapi/vendor', 				   	'SdapiController@API_getVendor');
Route::get('sdapi/item', 				   	'SdapiController@API_getItems');
Route::get('sdapi/category', 			   	'SdapiController@API_getCategory');
Route::get('sdapi/uom', 				   	'SdapiController@API_getUom');
Route::get('sdapi/pis', 				   	'SdapiController@API_getPriceItemSeq');
Route::get('sdapi/sys', 				   	'SdapiController@API_getSystemParams');
Route::get('sdapi/acp', 				   	'SdapiController@API_getAcPeriod');
Route::get('sdapi/currexch', 			   	'SdapiController@API_getCurrExchRate');
Route::get('sdapi/ilp', 				   	'SdapiController@API_getItemListPrice');
Route::get('sdapi/cgp', 				   	'SdapiController@API_getCustGrpItemPrice');
Route::get('sdapi/cp', 				   	   	'SdapiController@API_getCustPrice');
Route::get('sdapi/pp', 				        'SdapiController@API_getPeriodPrice');
Route::get('sdapi/all', 					'SdapiController@API_getAll');
Route::get('sdapi/test/{user_id}', 		    'SdapiController@test');


//Forecasting
Route::get('forecast/index', 				'ForecastController@index');
Route::get('forecast/create', 				'ForecastController@create');
Route::get('forecast/show/{id}', 			'ForecastController@show');
Route::get('forecast/edit/{id}', 			'ForecastController@edit');
Route::get('forecast/update/{id}', 			'ForecastController@update');
Route::get('forecast/delete', 				'ForecastController@delete');


//Order Placing
Route::get('orderplace/index', 				'PlaceorderController@index');
Route::get('orderplace/create',				'PlaceorderController@create');
Route::get('orderplace/show/{id}', 			'PlaceorderController@show');
Route::get('orderplace/edit/{id}', 			'PlaceorderController@edit');
Route::get('orderplace/update/{id}', 		'PlaceorderController@update');
Route::get('orderplace/delete/{id}', 		'PlaceorderController@delete');

























