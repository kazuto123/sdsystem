<?php namespace api\solutiondetail;

class ApiCustomer {

	protected $customer;
	protected $data;

	public function __construct(){}

	public function setCustomer($customer){
		$this->customer = $customer;
	}

	public function setData($data){
		$this->data = $data;
	}

	public function insertCustomer(){
		$this->customer->comp_code 			= $this->data->comp_code;
		$this->customer->cust_group_code	= $this->data->cust_group_code;
		$this->customer->cust_no			= $this->data->cust_no;
		$this->customer->cust_name 		 	= $this->data->cust_name;
		$this->customer->cust_cont_name 	= $this->data->cust_cont_name;
		$this->customer->cust_curr_code		= $this->data->cust_curr_code;
		$this->customer->cust_tax_type 		= $this->data->cust_tax_type;
		$this->customer->cust_absorb_tax	= $this->data->cust_absorb_tax;
		$this->customer->inactive 			= $this->data->inactive;
		$this->customer->salesman 			= $this->data->salesman;

		$this->customer->save();
	}

}