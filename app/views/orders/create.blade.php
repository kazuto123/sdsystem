@extends('layouts.master')
<!-- page title -->
@section('title')
@parent
	::Orders List
@stop

<!-- left navigation -->
@section('nav_left')
@include('includes.dp')
@stop

<!-- right navigation -->
@section('nav_right')
	<li><a href="{{ URL::to('/user/logout') }}">Logout</a></li>
@stop

<!-- main content -->
@section('content')
@include('includes.form_order')

@stop