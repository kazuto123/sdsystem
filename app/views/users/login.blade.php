@extends('layouts.master')

@section('title')
@parent
	::User Login
@stop

@section('content')
<div class="container" style="margin-top: 40px;">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 bc-login-form-container">
			<div class="bc-login-form-logo"> 
				{{ HTML::image('images/logo-konverge.png', 'test') }}
			</div>
			<div class="bc-login-form-title"> Konverge Order and Forecast System </div>
			
			<div class="bc-login-form">
				{{ Confide::makeLoginForm()->render() }}
			</div>
						
		</div>
	</div>
</div>


@stop