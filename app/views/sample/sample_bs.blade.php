@extends('layouts.master')

@section('content')
<h2>Bootstrap datepicker</h2>
@include('partials.datepicker')

<h2>Bootstrap Modal</h2>
@include('sample.modal_responsive')
<button class="demo btn btn-primary btn-lg" data-toggle="modal" href="#responsive">View Demo Responsive</button>

@include('sample.modal_stackable')
<button class="demo btn btn-primary btn-lg" data-toggle="modal" href="#stack1">View Demo Stackable</button>

@include('sample.modal_fullwidth')
<button class="demo btn btn-primary btn-lg" data-toggle="modal" href="#full-width">View Demo Full Width</button>

@include('sample.modal_longmodal')
<button class="demo btn btn-primary btn-lg" data-toggle="modal" href="#long">View Demo Long Modal</button>
@stop