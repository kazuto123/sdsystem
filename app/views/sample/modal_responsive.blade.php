<div id="responsive" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Responsive</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-6">
        <h4>Some Input</h4>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
      </div>
      <div class="col-md-6">
        <h4>Some More Input</h4>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
        <p><input class="form-control" type="text"></p>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
  </div>
</div>