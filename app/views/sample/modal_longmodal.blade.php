<div id="long" class="modal fade" tabindex="-1" data-replace="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">A Fairly Long Modal</h4>
  </div>
  <div class="modal-body">
    <button class="btn btn-default" data-toggle="modal" href="#notlong" style="position: absolute; top: 50%; right: 12px">Not So Long Modal</button>
    <img style="height: 800px" src="http://i.imgur.com/KwPYo.jpg">
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
  </div>
</div>