<!DOCTYPE html>
<html>
<head>
    <title>
        @section('title', 'Order Taking Apps')
        @show
    </title>
    @include('includes._head')
</head>

<body>
    <!-- Navigation Menu -->
    @include('includes._nav')
    <!-- Container -->
    <div class="container" style="margin-top: 60px;">
        <!-- Content -->
        @yield('content')
    </div>
    
    <!-- Footer -->
    <div class="container">
        @yield('footer')  
    </div>

    <!-- All Created JS script goes here -->
    @include('includes._script')
</body>
</html>