@extends('layouts.master')
<!-- page title -->
@section('title')
@parent
	::Orders List
@stop

<!-- left navigation -->
@section('nav_left')
@include('includes.dp')
@stop

<!-- right navigation -->
@section('nav_right')
	<li><a href="{{ URL::to('/user/logout') }}">Logout</a></li>
@stop

<!-- main content -->
@section('content')

@include('includes.control_bar')

<hr class="divider" style="margin-top: 5px; margin-bottom: 10px;">

@include('includes.order_list_all', array('orders'=>[1,2,3,4,5,6,7,8,9,10,11,12,13,14]))

@stop