<form class="form-horizontal container-fluid" role="form">
  
  <div class="row">
	@include('includes.form_details')
  </div> <!-- end of row 1 -->

  <hr class="divider" style="margin-top:5px; margin-bottom: 5px;">

  <div class="row">
  	<!-- Left -->
  	<div class="col-md-6">
  		<h4>Additional Order</h4>
  		@include('includes.category_list')
  		@include('includes.search')
  		@include('includes.category_items_list')	
  	</div>
  	
  	<!-- Right -->
  	<div class="col-md-6">
  		<h4>Preset Order</h4>

  		@include('includes.ordered_items_list')

  		<hr class="divider" style="margin-top: 5px; margin-bottom:5px;">

		  @include('includes.order_sum')
		
		  <hr class="divider" style="margin-top: 5px; margin-bottom:5px;">
		
  		<div class="row" style="">
        <div class="col-md-6 pull-right" style="margin-right:10px; margin-bottom: 5px;">
          <div class="row">
            <button class="btn btn-warning col-md-3 col-md-offset-2" type="button" style="margin-right: 3px; margin-top:3px; margin-bottom:3px;">Clear</button>
            <button class="btn btn-danger col-md-3" type="button" style="margin:3px;">Cancel</button>
            <button class="btn btn-primary col-md-3" type="button" style="margin:3px">Submit</button>                  
          </div>
        </div>
		  </div>
	  </div>

  </div> <!-- end of row -->
</form>