<li class="dropdown">
	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Order Taking<span class="caret"></span></a>
	<ul class="dropdown-menu">
		<li><a href="{{ URL::to('/orders/index') }}">Orders List</a></li>
		<li><a href="{{URL::to('/orders/create')}}">Take Order</a></li>			
	</ul>
</li>

<li class="dropdown">
	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Order Placing<span class="caret"></span></a>
	<ul class="dropdown-menu">
		<li><a href="{{ URL::to('/orderplace/index') }}">Orders List</a></li>
		<li><a href="{{URL::to('/orderplace/create')}}">Place Order</a></li>			
	</ul>
</li>

<li class="dropdown">
	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Forecast<span class="caret"></span></a>
	<ul class="dropdown-menu">
		<li><a href="{{ URL::to('/forecast/index') }}">Forecast List</a></li>
		<li><a href="{{ URL::to('/forecast/create') }}">Add Forecast</a></li>			
	</ul>
</li>
