<div class="row">
	<div class="col-md-offset-6 col-md-6">
		<!-- form sub total -->
		<div class="form-group">
			<label for="customerGroup" class="col-md-5 control-label">Sub Total</label>
			<div class="col-md-7">
			  <input type="input" class="form-control input-sm" id="form_subtotal" placeholder="$XXXXXX.XX" disabled>
			</div>
		</div>
		<!-- form gst amount -->
		<div class="form-group">
		    <label for="customerGroup" class="col-md-5 control-label">Add GST 7%:</label>
		    <div class="col-md-7">
		      <input type="input" class="form-control input-sm" id="form_gst" placeholder="$XXXXXX.XX" disabled >
		    </div>
		</div>
		<!-- form total -->
		<div class="form-group">
		    <label for="customerGroup" class="col-md-5 control-label">Total</label>
		    <div class="col-md-7">
		      <input type="input" class="form-control input-sm" id="form_total" placeholder="$XXXXXX.XX" disabled>
		    </div>
		</div>
	</div>
</div>