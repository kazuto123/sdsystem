<!-- Left Column -->
<div class="col-md-6">
  <!-- Customer Group Form -->	
  <div class="form-group">
    <label for="input_customer_group" class="col-md-4 control-label">Customer Group</label>
    <div class="col-md-8">
      <input type="input" class="form-control input-sm" id="input_customer_group" placeholder="Customer Group">
    </div>
  </div>

  <!-- Outlet Code -->
  <div class="form-group">
    <label for="input_outlet_code" class="col-md-4 control-label">Outlet Code</label>
    <div class="col-md-8">
      <input type="password" class="form-control input-sm" id="input_outlet_code" placeholder="Key in Outlet No.">
    </div>
  </div>

  <!-- Contact Person -->
  <div class="form-group">
    <label for="input_contact_person" class="col-md-4 control-label">Contact Person</label>
    <div class="col-md-8">
      <input type="password" class="form-control input-sm" id="input_contact_person" placeholder="Contact Person Name">
    </div>
  </div>

  <!-- Order Number -->
  <div class="form-group">
    <label for="input_order_number" class="col-md-4 control-label">Order Number</label>
    <div class="col-md-8">
      <input type="password" class="form-control input-sm" id="input_order_number" placeholder="Order No.">
    </div>
  </div>
</div>

<!-- right Column -->
<div class="col-md-6">
  <!-- Delivery Address Form -->
  <div class="form-group">
    <label for="input_delivery_address" class="col-md-4 control-label">Delivery Address</label>
    <div class="col-md-8">
      <input type="input" class="form-control input-sm" id="input_delivery_address" placeholder="Enter Delivery Address">
    </div>
  </div>

  <!-- Delivery Driver Form -->
  <div class="form-group">
    <label for="inputPassword3" class="col-md-4 control-label">Desinated Driver</label>
    <div class="col-md-8">
      @include('includes.selectpicker', array('options'=>['Driver1', 'Driver2', 'Driver3']))
    </div>
  </div>

  <!-- Delivery Date Form -->
  <div class="form-group">
    <label for="input_delivery_datetime" class="col-md-4 control-label">Delivery Date</label>
    <div class="col-md-8">
        <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control" id="input_delivery_datetime">
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>

    </div>
  </div>
</div> 	
