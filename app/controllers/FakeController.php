<?php

class FakeController extends BaseController {

	public function showOrderIndex(){
		return View::make('orders.index')
			->with('nav_left_buttons', 
				'<li class="active"><a href=\"\{\{ URL::to(\"/orders/index\") \}\}\">Orders List<\/a><\/li>
				 <li><a href="\{\{URL::to("/orders/create")\}\}">Place Order</a></li>')
			->with('nav_right_buttons', 
				'<li><a href="{{ URL::to("/user/logout") }}">Logout</a></li>');		
	}

	public function showCreateOrderForm(){
		return View::make('orders.create')
			->with('nav_left_buttons', 
				'<li ><a href="{{ URL::to("/orders/index") }}">Orders List</a></li>
				 <li class="active"><a href="{{URL::to("/orders/create")}}">Place Order</a></li>')
			->with('nav_right_buttons', 
				'<li><a href="{{ URL::to("/user/logout") }}">Logout</a></li>');
	}

}