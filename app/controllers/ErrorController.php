<?php

class ErrorController extends BaseController{

	//constructor
	public function __construct() {

	}

	// redirect user to not Not Login Page
	public function showNotLogin(){
		return View::make('error.notlogin');
	}

	// redirect user to Not Accessible Page 
	public function showNotAccessible(){
		return View::make('error.notaccessible');
	}

	//redirect user to Page Not Found Page
	public function showPageNotFound(){
		return View::make('error.pagenotfound');
	}
}