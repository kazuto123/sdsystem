<?php

class RoleController extends BaseController {

	protected $user;
	protected $role;
	protected $permission;

	public function __construct(User $user, Role $role, Permission $permission) {
		$this->user = $user;
		$this->role = $role;
		$this->permission = $permission;
	}

	//create User role by URL parameter Passing
	public function createRole($rolename){
		$this->role->name = $rolename;
		$this->role->save();

		return $this->role->where('name','=',$rolename)->first()->name . ' Created Successfully!!';
	}

	//create role permission by URL parameter Passing 
	public function createPermission($perm_name, $perm_display_name){
		$this->permission->name = $perm_name;
		$this->permission->display_name = $perm_display_name;
		$this->permission->save();

		return $this->permission->where('name','=',$perm_name)->first()->name . ' Created Successfully';
	}

	//Assigned user to role using parameter passing by URL (/role/assign/username/rolename)
	public function assignRole( $username, $rolename ){
		$assignedUser = $this->user->where('username', '=', $username)->first();
		$assignedRole = $this->role->where('name', '=', $rolename)->first();
		
		$assignedUser->attachRole ($assignedRole);

		return 'User:' . $assignedUser . ' Assigned With Role: ' . $assignedRole;
	}

	//assigned permission to role (perm/assign/RoleName/PermissionName)
	public function assignPerm2Role( $rolename, $perm_name){
		$assignedRole = $this->role->where('name', '=', $rolename)->first();
		$assignedPerm = $this->permission->where('name', '=', $perm_name)->first();

		$assignedRole->perms()->sync(array($assignedPerm->id));

		return 'OK';
	}

}