<?php

class SdapiController extends BaseController{

	protected $login = 'kremote:kremote132';
	public function __construct(){}


	public function test($user_id){
		
		$cust = Customer::find($user_id)->address()->get();
		echo $cust;
			
		//return $cust;
	}

	//function to obtained data give the API URI
	private function getData($apiurl){
		$json_string = file_get_contents($apiurl);
		return json_decode($json_string);
	}

	//Insert the user based on the provide field to the users table
    private function insertUser($email, $username, $password, $issalesman, $role, $id){
	    $found = User::where('username', '=', $username)->first();
	    if($found){
	    	echo 'username existed.. <br>';
	    }
	    else{
		    $user = new User;
	        $user->username = $username;
	        $user->email = $email;
	        $user->password = $password;
	        $user->issalesman = $issalesman;
	        $user->user_role = $role;
	        $user->user_role_id = $id; //their id no. eq. vend_no, cust_no, admin_no, etc...
	        $user->confirmed = 1;

	        $user->password_confirmation = $password;

	        $done = $user->save();        	
	        //echo $done;
	        if ($done){
	            echo 'Successfully added user: ' . $username . ' to user table. <br>';
	        }
	        else{
	            return $user->errors()->all(':message') . '<br>';
	        }	    	
	    }

	}

	//function to insert Customer Data from Solution Detail API
	public function API_getCustomer(){
		//$json_string = file_get_contents('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_ar_customer?compcode=01');
        //$content = json_decode($json_string);
        $content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_ar_customer?compcode=01');
        foreach ($content as &$item){  
        	$found = Customer::where('cust_no', '=', $item->cust_no)->first();
        	if($found){
        		echo $item->cust_no . 'Exist in Database. Not inserted ...' . '<br>';
        	}
        	else{
	        	$cust = new Customer();
	        	$cust->id 					= $item->cust_no;
	        	$cust->comp_code 			= $item->comp_code;
	        	$cust->cust_group_code		= $item->cust_group_code;
	        	$cust->cust_no				= $item->cust_no;
	        	$cust->cust_name 		 	= $item->cust_name;
	        	$cust->cust_cont_name 		= $item->cont_name;
	        	$cust->cust_curr_code		= $item->curr_code;
	        	$cust->cust_tax_type 		= $item->tax_type;
	        	$cust->cust_absorb_tax		= $item->absorb_tax;
	        	$cust->inactive 			= $item->inactive;
	        	$cust->salesman 			= $item->salesman;

	        	$cust->save();        		
        		echo 'NEW Customer No:' . $item->cust_no . 'Inserted.' . '<br>';

        		//$this->insertUser('cust_'. $item->cust_no . '@gmail.com' ,'cust_' . $item->cust_no, $item->cust_no, 0, 'customers', $item->cust_no);
				  		
        	}
        }	        
	    return 'DONE Inserting All Customers From API -- <br>';
	}


	//function to obtain customer_address data from Solution Detail API -- No Duplicate Checkings!!
	public function API_getCustAddr(){
		$json_string = file_get_contents('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_ar_cust_addr?compcode=01');
        $content = json_decode($json_string);
        foreach ($content as &$item){  
        	$addr = new Customeraddr();
        	$addr->comp_code 	= $item->comp_code;
        	$addr->cust_no 		= $item->cust_no;
        	$addr->addr_ref 	= $item->addr_ref;
        	$addr->addr 		= $item->addr;
        	$addr->zip 			= $item->zip;
        	$addr->ctry_code 	= $item->ctry_code;
        	$addr->cont_name 	= $item->cont_name;
        	$addr->cont_off_tel = $item->cont_off_tel;
        	$addr->cont_res_tel	= $item->cont_res_tel;
        	$addr->cont_hp_no	= $item->cont_hp_no;
        	$addr->cont_fax_no  = $item->cont_fax;
        	$addr->cont_email	= $item->cont_email;
        	$addr->active 		= 1;

        	$done = $addr->save();
        	echo 'Custommer Addr Zip:' . $item->zip . ' -Is Inserted <br>';
        }

        if($done) return 'All Customer_addr data Inserted <br>';
        return 'Something is wrong!! not updated!! <br>';
	}

	//Get Vendor Informations from Solution detail API
	public function API_getVendor(){
        $content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_ap_vendor?compcode=01');
        foreach ($content as &$item){  
        	$found = Vendor::where('vend_no', '=', $item->vend_no)->first();
	        if($found){
	        	echo 'Vendor No: ' . $item->vend_no . " Exist in Database. Not Inserted." . '<br>';
	        }
	        else{
	        	$vendor 					= new Vendor();
	        	$vendor->id 				= $item->vend_no;
	        	$vendor->comp_code 			= $item->comp_code;
	        	$vendor->vend_grp_code 		= $item->vend_grp_code;
	        	$vendor->vend_no 			= $item->vend_no;
	        	$vendor->vend_name 			= $item->vend_name;
	        	$vendor->curr_code 			= $item->curr_code;
	        	$vendor->tax_type 			= $item->tax_type;
	        	$vendor->addr 	  			= $item->addr;
	        	$vendor->ctry_code 			= $item->ctry_code;
	        	$vendor->cont_name 			= $item->cont_name;
	        	$vendor->phone_no1			= $item->phone_no1;
	        	$vendor->phone_no2 			= $item->phone_no2;
	        	$vendor->fax_no 			= $item->fax_no;
	        	$vendor->cont_email			= $item->cont_email;

	        	$done = $vendor->save();

	        	echo "New Vendor No. :" . $item->vend_no . "Inserted <br>";

	        }
       	}
       	return 'All Vendors Insertion from API are Done!! <br>';
	}

	//function to Get Items from Solution Detail API
	public function API_getItems(){
        $content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_in_item?compcode=01');

        foreach ($content as &$i){  
	        $found = Item::where('item_no', '=', $i->item_no)->first();
	        if($found){
	        	echo "Item No: " . $i->item_no . " Existed, Not Inserted. <br>";
	        }
	        else{
	        	$item = new Item();
	        	$item->id 					= $i->item_id;
	        	$item->comp_code 			= $i->comp_code;
	        	$item->item_no 				= $i->item_no;
	        	$item->item_descr		 	= $i->item_descr;
	        	$item->cat_code				= $i->cat_code;
	        	$item->uom 					= $i->uom;
	        	$item->is_sell				= $i->is_sell;
	        	$item->is_buy 				= $i->is_buy;
	        	$item->is_inventory			= $i->is_inventory;
	        	$item->flang_descr			= $i->flang_descr;

	        	$item->save();

	        	echo "New Item No: " . $i->item_no . "Inserted. <br>";
	        }
    	}
        return 'Finished Get Item ...... <br>';
	}

	//get category from the API
	public function API_getCategory(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_in_sub_cat?compcode=01');
		
		foreach ($content as &$i){

			$cat = new Category();
			$cat->comp_code 			= $i->comp_code;
			$cat->cat_pos 				= $i->cat_pos;
			$cat->cat_len 				= $i->cat_len;
			$cat->sub_cat 				= $i->sub_cat;
			$cat->descr 				= $i->descr;

			$done = $cat->save();
			if($done){
				echo 'New Category: ' . $i->descr . 'Inserted. <br>';	
			}
			else{
				echo $cat->errors()->all(':message') . '<br>';
			}

		}
		return 'Finished Get Catogory .... <br>';
	}

	//get UOM from API -- No Duplicate checkings ...
	public function API_getUom(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_in_item_uom_dtls?compcode=01');
		foreach ($content as &$i){
			$uom = new Uom();
			$uom->comp_code 				= $i->comp_code;
			$uom->item_id 					= $i->item_id;
			$uom->s_no 						= $i->s_no;
			$uom->uom 						= $i->uom;
			$uom->cf_qty 					= $i->cf_qty;
			$uom->uom_basic					= $i->uom_basic;

			$done = $uom->save();

			if($done){
				echo 'Inserted:' . $i->uom . ' - into uom table <br>';
			}
			else{
				echo $uom->errors()->all(':message') . '<br>';
			}
		}
		return 'Finished get UOM ....';
	}

	//get price item seq
	public function API_getPriceItemSeq(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_in_item_price_seq?compcode=01');
		foreach ($content as &$i){
			$pis = new PriceItemSeq();

			$pis->comp_code     			= $i->comp_code;
			$pis->price_type				= $i->price_type;
			$pis->seq 						= $i->seq;

			$done = $pis->save();
			if($done){
				echo 'Inserted: ' . $i->price_type . ' - into price Item Seq table. <br>';
			}
			else{
				echo $pis->errors()->all(':message') . '<br>';
			}
		}
		return 'Finished get Price item seq ... <br>';
	}
	
	
	//get system params
	public function API_getSystemParams(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_sys_system_param?compcode=01');
		foreach($content as &$i){
			$sys = new SysParam();
			$sys->comp_code 				= $i->comp_code;
			$sys->param_id 					= $i->param_id;
			$sys->param_value 				= $i->param_value;
			$sys->remark 					= $i->remarks;

			$done = $sys->save();
			if($done){
				echo 'Inserted: ' . $i->param_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $sys->errors()->all(':message') . '<br>';
			}
		}
		return 'Finished get sys param ... <br>';
	}

	//get AC_period
	public function API_getAcPeriod(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_gl_ac_period?compcode=01');
		foreach($content as &$i){
			$acp = new AcPeriod();

			$acp->comp_code 				= $i->comp_code;
			$acp->ac_year 					= $i->ac_year;
			$acp->ac_period 				= $i->ac_period;
			$acp->st_date 					= $i->st_date;
			$acp->ed_date 					= $i->ed_date;
			$acp->status 					= $i->status;

			$done = $acp->save();
			if($done){
				echo 'Inserted: ' . $i->ac_period . ' - into price Item Seq table. <br>';
			}
			else{
				echo $acp->errors()->all(':message') . '<br>';
			}			
		}
		return 'Finished get Ac Period ... <br>';
	}

	//get user master .... SKIP .....


	//get currency Exchange info ...  API got issue ---
	public function API_getCurrExchRate(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_currency_exch?compcode=01');
		
		foreach($content as &$i){

			$cer = new CurrExchRate();

			$cer->comp_code  				= $i->comp_code;
			$cer->curr_code 				= $i->curr_code;
			$cer->ac_period  				= $i->ac_period;
			$cer->exch_rate 				= $i->exch_rate;

			$done = $cer->save();
			if($done){
				echo 'Inserted: ' . $i->curr_code . ' - into price Item Seq table. <br>';
			}
			else{
				echo $cer->errors()->all(':message') . '<br>';
			}						
		}
		return 'Finished get Currency Exchange Rate ... <br>';
	}

	//get item list price
	public function API_getItemListPrice(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_in_item_list_price?compcode=01');
		
		foreach ($content as &$i){
			$ilp = new ItemListPrice();
			$ilp->comp_code  				= $i->comp_code;
			$ilp->item_id  					= $i->item_id;
			$ilp->uom 						= $i->uom;
			$ilp->price 					= $i->price;
			$done = $ilp->save();
			if($done){
				echo 'Inserted: ' . $i->item_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $ilp->errors()->all(':message') . '<br>';
			}					

		}
		return 'Finished get Item List Price ';
	}


	//get cust_grp_price ....
	public function API_getCustGrpItemPrice(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_in_item_custgrp_price?compcode=01');
		foreach($content as &$i){
			$cgp = new CustGrpPrice();
			$cgp->comp_code 				= $i->comp_code;
			$cgp->item_id 					= $i->item_id;
			$cgp->cust_group_code			= $i->cust_group_code;
			$cgp->uom 						= $i->uom;
			$cgp->price 					= $i->price;
			$done = $cgp->save();
			if($done){
				echo 'Inserted: ' . $i->item_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $cgp->errors()->all(':message') . '<br>';
			}		
		}
		return 'Finished get Cust Group item price ... <br>';
	}

	//get customer price
	public function API_getCustPrice(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_in_item_cust_price?compcode=01');
		foreach($content as &$i){
			$cp = new CustPrice();
			$cp->comp_code 					= $i->comp_code;
			$cp->item_id 					= $i->item_id;
			$cp->cust_no					= $i->cust_no;
			$cp->uom 						= $i->uom;
			$cp->price 						= $i->price;
			$done = $cp->save();
			if($done){
				echo 'Inserted: ' . $i->item_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $cp->errors()->all(':message') . '<br>';
			}		
		}
		return 'Finished get Cust item price ... <br>';		
	}

	//get period price
	public function API_getPeriodPrice(){
		$content = $this->getData('http://kremote:kremote132@203.125.118.241:8080/konvergefb/web_in_item_period_price?compcode=01');
		foreach($content as &$i){
			$pp = new PeriodPrice();
			$pp->comp_code 					= $i->comp_code;
			$pp->item_id 					= $i->item_id;
			$pp->cust_group_code			= $i->cust_group_code;
			$pp->cust_no 					= $i->cust_no;
			$pp->uom 						= $i->uom;
			$pp->s_no   					= $i->s_no;
			$pp->from_date 					= $i->from_date;
			$pp->to_date 					= $i->to_date;
			$pp->price 						= $i->price;
			$done = $pp->save();
			
			if($done){
				echo 'Inserted: ' . $i->item_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $pp->errors()->all(':message') . '<br>';
			}		
		}
		return 'Finished get Period item price ... <br>';
	}

	public function API_getAll(){
		$this->API_getCustomer();
		$this->API_getCustAddr();
		$this->API_getVendor();
		$this->API_getItems();
		$this->API_getCategory();
		$this->API_getUom();
		$this->API_getPriceItemSeq();
		$this->API_getSystemParams();
		$this->API_getAcPeriod();
		$this->API_getCurrExchRate();
		$this->API_getItemListPrice();
		$this->API_getCustGrpItemPrice();
		$this->API_getCustPrice();
		$this->API_getPeriodPrice();

		return 'DONE !! ---------';
	}



}