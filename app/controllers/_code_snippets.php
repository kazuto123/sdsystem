<?php

//--- for dynamically adding the .active class to nav
HTML::macro('clever_link', function($route, $text) {	
	if( Request::path() == $route ) {
		$active = "class = 'active'";
	}
	else {
		$active = '';
	}
 
  return '<li ' . $active . '>' . link_to($route, $text) . '</li>';
});

/*
in blade, call this.
<ul>
    <li>{{ HTML::clever_link("/", 'Home' ) }}</li>
    <li>{{ HTML::clever_link("blog", 'Blog' ) }}</li>
</ul>
*/

//--------
